/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.exceptionHandlingRelated;

import pt.cpx.dto.GenericExceptionResponseDTO;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.glassfish.jersey.server.ParamException.PathParamException;
import org.glassfish.jersey.server.ParamException.QueryParamException;
import static pt.cpx.utils.Utils.MALFORMED_JSON_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.MALFORMED_JSON_MSG;
import static pt.cpx.utils.Utils.PATH_PARAM_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.PATH_PARAM_EX_MSG;
import static pt.cpx.utils.Utils.QRY_PARAM_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.QRY_PARAM_EX_MSG;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

    public Response toResponse(Throwable ex) {

        Response.ResponseBuilder b = null;
        if (ex instanceof GenericException) {
            int httpCode = ((GenericException) ex).getHttpStatusCode();
            b = Response.status(httpCode).entity(new GenericExceptionResponseDTO(((GenericException) ex).getErrorMessage()));

        } else if (ex instanceof NotFoundException || ex instanceof PathParamException) {
            // do tipo resourse/1/AAA (n existe barra AAA) ou resourse/1A( espera int)
            b = Response.status(PATH_PARAM_EX_HTTP_STATUS_CODE).entity(new GenericExceptionResponseDTO(PATH_PARAM_EX_MSG));

        } else if (ex instanceof QueryParamException) {
            // ex: qd espera um int e vem c 1A ou Y
            b = Response.status(QRY_PARAM_EX_HTTP_STATUS_CODE).entity(new GenericExceptionResponseDTO(QRY_PARAM_EX_MSG));

        } else if (ex instanceof ProcessingException) { // ao receber JSON mal formado
            b = Response.status(MALFORMED_JSON_HTTP_STATUS_CODE).entity(new GenericExceptionResponseDTO(MALFORMED_JSON_MSG));
        } else {
            int httpCode = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
            b = Response.status(httpCode).entity(new GenericExceptionResponseDTO(ex.getMessage()));

        }

        if (ex.getCause() != null) { //erro n gerado explicitamente pelo developer
            ex.printStackTrace();
        }

        return b.type(MediaType.APPLICATION_JSON).build();
    }
}
