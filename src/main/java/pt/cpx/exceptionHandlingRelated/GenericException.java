/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.exceptionHandlingRelated;

/**
 *
 * @author helder.dinis
 */
public class GenericException extends Exception {

    int httpStatusCode;
    String errorMessage;
    String className;

    public GenericException(int httpStatusCode, String errorMessage, Throwable cause, String className) {
        super(cause);
        this.errorMessage = errorMessage;
        this.httpStatusCode = httpStatusCode;
        this.className = className;

    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
