/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.utils;

/**
 *
 * @author Ulisses Pinto
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import pt.cpx.exceptionHandlingRelated.GenericException;

public class Utils {

    public static String INVALID_DATE_EX_MSG = "Formato de data inválido";
    public static int INVALID_DATE_EX_STATUS_CODE = 400;

    public static String NUMERIC_EXPECTED_EX_MSG = "Foi encontrado um caracter não numérico onde era esperado um numérico(data ou numero)";
    public static int NUMERIC_EXPECTED_EX_STATUS_CODE = 400;

    public static String ID_NOT_FOUND_EX_MSG = "Registo não encontrado";
    public static int ID_NOT_FOUND_EX_STATUS_CODE = 400;

    public static String REQUIRED_FIELD_EX_MSG = "Confira campo(s) obrigatório(s)";
    public static int REQUIRED_FIELD_EX_HTTP_STATUS_CODE = 400;

    public static String DAO_EX_MSG = "Erro no DAO";
    public static int DAO_EX_HTTP_STATUS_CODE = 400;    
    
    public static String GET_DATA_EX_MSG = "Erro ao interrogar a bd";
    public static int GET_DATA_EX_HTTP_STATUS_CODE = 400;

    public static String CTRL_EX_MSG = "Erro no controller";
    public static int CTRL_EX_HTTP_STATUS_CODE = 400;

    public static String DB_CONN_EX_MSG = "Não foi possível obter ligação à base de dados";
    public static int DB_CONN_EXC_HTTP_STATUS_CODE = 500;

    public static String UNAUTHORIZED_MSG = "Não tem permissão";
    public static int UNAUTHORIZED_HTTP_STATUS_CODE = 401;

    public static String QRY_PARAM_EX_MSG = "Query parameter(s) em falta ou aquele que foi indicado não é o esperado";
    public static int QRY_PARAM_EX_HTTP_STATUS_CODE = 400;

    public static String PATH_PARAM_EX_MSG = "End Point não definido ou o tipo de parâmetro indicado não é o esperado";
    public static int PATH_PARAM_EX_HTTP_STATUS_CODE = 404;

    public static String MALFORMED_JSON_MSG = "JSON mal formado";
    public static int MALFORMED_JSON_HTTP_STATUS_CODE = 400;

    public Utils() {
    }

    public static java.sql.Connection getConnectionCPX() throws Exception {
        Context c = new InitialContext();
        try {
            DataSource d = (DataSource) c.lookup("jdbc/app_cpx");
            //d.getConnection().getMetaData(); // checar qq coisa p saber se bd está acessivel
            return d.getConnection();

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DB_CONN_EX_MSG, ex, Utils.class.getSimpleName());
            }
        }

    }

    public static boolean idExists(Long id, String tn, java.sql.Connection c) throws Exception {
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        boolean exists = false;
        try {

            String q = "select id from app_cpx." + tn + " where id = ?";

            ps = c.prepareStatement(q);
            ps.setLong(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                exists = true;
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(500, "Erro ao verificar se o ID existe", ex, "Utils");
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return exists;
    }

    public  void setParams(PreparedStatement pstmt, Object[] params) throws SQLException {
        if (params != null) {
            final int tam = params.length;
            for (int i = 1; i < tam; i++) {
                Object param = params[i];
                //if (param != null) {
                pstmt.setObject(i, param);
                //}
            }
        }
    }

    public  ResultSet query(PreparedStatement ps, Object[] params) throws SQLException, Exception {
        setParams(ps, params);
        return ps.executeQuery();
    }

    public  List getData(String q, Object[] params) throws SQLException, Exception {
        java.sql.Connection c = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List rows = new ArrayList<Map<String, Object>>();
     
        try {
            c = Utils.getConnectionCPX();
            ps = c.prepareStatement(q);
            rs = this.query(ps, params);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            while (rs.next()) {
                Map<String, Object> row = new LinkedHashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String name = rsmd.getColumnName(i);
                    row.put(name, rs.getObject(name));
                }
                rows.add(row);
            }
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(GET_DATA_EX_HTTP_STATUS_CODE, GET_DATA_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (c != null) {
                c.close();
            }

        }
        return rows;
    }
}
