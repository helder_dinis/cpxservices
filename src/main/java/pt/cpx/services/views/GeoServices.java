package pt.cpx.services.views;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.List;
import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import pt.cpx.ctrl.CpxCTRL;
import pt.cpx.dto.GenericExceptionResponseDTO;
import pt.cpx.dto.GenericResponseGetDTO;
import pt.cpx.dto.GenericResponseGetPaginationDTO;
import pt.cpx.interfaces.CpxINTERFACE;

@Path("/radioNetworks")
@Api(value = "GeoServices", authorizations = {
    @Authorization(value = "sampleoauth", scopes = {})})
public class GeoServices {

    @Context
    private UriInfo context;

    @GET
    @ApiOperation(value = "Retorna lista", notes = "",
            response = GenericResponseGetDTO.class,
            responseContainer = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok"),
        @ApiResponse(code = 400, message = "Bad Request", response = GenericExceptionResponseDTO.class)})
    @Produces({"application/json; charset=UTF-8"})
    public Response getNetwork(@ApiParam(value = "Valores possíveis: ROB/SIRESP/REPC", required = true, defaultValue = "SIRESP") @QueryParam("network") String network) throws Exception {
        Response.ResponseBuilder response = null;
        CpxINTERFACE listNetwork = new CpxCTRL();
        List<Map<String, Object>> ln = listNetwork.LR(network);

        GenericResponseGetDTO resp = new GenericResponseGetDTO();
        GenericResponseGetPaginationDTO pag = new GenericResponseGetPaginationDTO();

        pag.setTotalRows(ln.size());
        resp.setData(ln);
        resp.setPagination(pag);

        response = Response.ok(resp);

        return response.build();
    }
    // http://localhost:8080/cpxServices/webresources/radioNetworks?network=SIRESP

}
