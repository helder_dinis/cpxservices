/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.services.views;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import pt.cpx.ctrl.CpxCTRL;
import pt.cpx.dto.GenericExceptionResponseDTO;
import pt.cpx.dto.GenericResponseGetDTO;
import pt.cpx.dto.GenericResponseGetPaginationDTO;
import pt.cpx.interfaces.CpxINTERFACE;

/**
 * REST Web Service
 *
 * @author helder.dinis
 */
@Path("/recursos")
@Api(value = "Recursos", authorizations = {
    @Authorization(value = "sampleoauth", scopes = {})})
public class Recursos {

    @Context
    private UriInfo context;

    public Recursos() {
    }

    @GET
    @Path("/{id}/foto")
    @ApiOperation(value = "Retorna a foto do recurso", notes = "",
            response = GenericResponseGetDTO.class,
            responseContainer = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok"),
        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
    public Response getFoto(@ApiParam(value = "", required = true, defaultValue = "11") @PathParam("id") int id) throws Exception {
        Response.ResponseBuilder response = null;
        CpxINTERFACE ctrl = new CpxCTRL();
        Map<String, Object> img = ctrl.LRF(id);

        response = Response.ok(img.get("bin"));
        response.header("Content-Type", img.get("mimeType") + "; charset=utf-8");
        //response.header("Content-Disposition", "attachment;filename=dd.jpeg");

        return response.build();
    }
}
