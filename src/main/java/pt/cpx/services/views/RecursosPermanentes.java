/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.services.views;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import pt.cpx.ctrl.CpxCTRL;
import pt.cpx.dto.GenericExceptionResponseDTO;
import pt.cpx.dto.GenericResponseGetDTO;
import pt.cpx.dto.GenericResponseGetPaginationDTO;
import pt.cpx.dto.update.RecursoPermanenteUpdDTO;
import pt.cpx.interfaces.CpxINTERFACE;

/**
 * REST Web Service
 *
 * @author helder.dinis
 */
@Path("/recursosPermanentes")
@Api(value = "Recursos Permanentes", authorizations = {
    @Authorization(value = "sampleoauth", scopes = {})})
public class RecursosPermanentes {

    @Context
    private UriInfo context;

    public RecursosPermanentes() {
    }

    //http://localhost:8080/cpxServices/webresources/recursosPermanentes/1
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Retorna um registo", notes = "",
            response = GenericResponseGetDTO.class,
            responseContainer = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok"),
        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
    @Produces({"application/json; charset=UTF-8"})
    public Response getRecurso_Permanente(@PathParam("id") int id) throws Exception {
        Response.ResponseBuilder response = null;
        CpxINTERFACE ctrl = new CpxCTRL();
        List ln = ctrl.LRP(id);

        GenericResponseGetDTO resp = new GenericResponseGetDTO();
        GenericResponseGetPaginationDTO pag = new GenericResponseGetPaginationDTO();

        pag.setTotalRows(ln.size());
        resp.setData(ln);
        resp.setPagination(pag);

        response = Response.ok(resp);

        return response.build();
    }

    @POST
    @ApiOperation(value = "Adiciona um registo", notes = "", responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Ok"),
        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
    public Response criarRecursoPermanente(@Context UriInfo uriInfo, @ApiParam(value = "Dados a submeter", required = true) pt.cpx.dto.create.RecursoPermanenteInsDTO row) throws Exception {
        Response.ResponseBuilder response = null;
        CpxINTERFACE ctrl = new CpxCTRL();
        Long id = ctrl.CRP(row);

        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Long.toString(id)); // set Header Location
        response = Response.created(builder.build());

        return response.build();
    }

    @PUT
    @Path("/{id}")
    @ApiOperation(value = "Altera um registo", notes = "No body, indicar apenas os atributos a alterar", responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Ok"),
        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
    public Response alterarRecursoPermanente(@Context UriInfo uriInfo, @PathParam("id") Long id, @ApiParam(value = "Dados a submeter", required = true) RecursoPermanenteUpdDTO row) throws Exception {
        Response.ResponseBuilder response;
        row.setId(id);
        CpxINTERFACE ctrl = new CpxCTRL();
        ctrl.URP(row);

        response = Response.status(Response.Status.NO_CONTENT); //204

        return response.build();
    }
    
    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Elimina um registo")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Ok"),
        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
    public Response apagarRecursoPermanente(@PathParam("id") Long id) throws Exception {
        Response.ResponseBuilder response;
        CpxINTERFACE ctrl = new CpxCTRL();
        ctrl.ARP(id);

        response = Response.status(Response.Status.NO_CONTENT); //204

        return response.build();
    }    
}
