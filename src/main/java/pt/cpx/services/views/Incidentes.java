/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.services.views;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import pt.cpx.ctrl.CpxCTRL;
import pt.cpx.dto.GenericExceptionResponseDTO;
import pt.cpx.dto.GenericResponseGetDTO;
import pt.cpx.dto.GenericResponseGetPaginationDTO;
import pt.cpx.interfaces.CpxINTERFACE;

/**
 * REST Web Service
 *
 * @author helder.dinis
 */
@Path("/incidentes")
@Api(value = "Incidentes", authorizations = {
    @Authorization(value = "sampleoauth", scopes = {})})
public class Incidentes {

    @Context
    private UriInfo context;

    public Incidentes() {
    }

    @GET
    @ApiOperation(value = "Retorna uma lista", notes = "", response = GenericResponseGetDTO.class,responseContainer = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok"),
        @ApiResponse(code = 400, message = "Bas Request", response = GenericExceptionResponseDTO.class)})
    @Produces({"application/json; charset=UTF-8"})
    public Response getIncidentes(@ApiParam(value = "", required = true, defaultValue = "1")  @QueryParam("exercicio_id") int exercicio_id) throws Exception {
        Response.ResponseBuilder response = null;
        CpxINTERFACE ctrl = new CpxCTRL();
        List ln = ctrl.LI(exercicio_id);

        GenericResponseGetDTO resp = new GenericResponseGetDTO();
        GenericResponseGetPaginationDTO pag = new GenericResponseGetPaginationDTO();

        pag.setTotalRows(ln.size());
        resp.setData(ln);
        resp.setPagination(pag);

        response = Response.ok(resp);

        return response.build();
    }
}
