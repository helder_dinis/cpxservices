/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.services.views;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import pt.cpx.ctrl.CpxCTRL;
import pt.cpx.dto.GenericExceptionResponseDTO;
import pt.cpx.dto.GenericResponseGetDTO;
import pt.cpx.dto.GenericResponseGetPaginationDTO;
import pt.cpx.interfaces.CpxINTERFACE;

/**
 * REST Web Service
 *
 * @author helder.dinis
 */
@Path("/recursosDisponiveis")
@Api(value = "Recursos Disponíveis", authorizations = {
    @Authorization(value = "sampleoauth", scopes = {})})
public class RecursosDisponiveis {

    @Context
    private UriInfo context;

    public RecursosDisponiveis() {
    }

    @GET
    @ApiOperation(value = "Retorna uma lista dos recrursos disponíveis", notes = "Os critérios são cumulativos",
            response = GenericResponseGetDTO.class,
            responseContainer = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok"),
        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
    @Produces({"application/json; charset=UTF-8"})
    public Response getRecursoDisponivel(@Context UriInfo uriInfo,@ApiParam(value = "", required = true, defaultValue = "1") @QueryParam("exercicio_id") int exercicio_id, @ApiParam(value = "", required = false, defaultValue = "1") @QueryParam("tipo_recurso_id") int tipo_recurso_id, @ApiParam(value = "Designação do recurso(VUCI/ABS/...)", required = false, defaultValue = "VUC") @QueryParam("recurso") String recurso, @ApiParam(value = "Uma ou várias, separadas por vírgula(VIAT,ENT)", required = false, defaultValue = "VIAT") @QueryParam("siglas") String siglas) throws Exception {
        Response.ResponseBuilder response = null;
        CpxINTERFACE ctrl = new CpxCTRL();
        List ln = ctrl.LRD(uriInfo,exercicio_id, tipo_recurso_id, recurso, siglas);

        GenericResponseGetDTO resp = new GenericResponseGetDTO();
        GenericResponseGetPaginationDTO pag = new GenericResponseGetPaginationDTO();

        pag.setTotalRows(ln.size());
        resp.setData(ln);
        resp.setPagination(pag);

        response = Response.ok(resp);

        return response.build();
    }

    
    
    
    
//    @GET
//    @Path("/{id}")
//    @ApiOperation(value = "Retorna uma lista em função de uma descrição", notes = "",
//            response = GenericResponseGetDTO.class,
//            responseContainer = "")
//    @ApiResponses(value = {
//        @ApiResponse(code = 200, message = "Ok"),
//        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
//    @Produces({"application/xml; charset=UTF-8"})
//    public Response getRecursoDisponivelDesc(@ApiParam(value = "", required = true, defaultValue = "1") @QueryParam("recurso") String recurso, @ApiParam(value = "Separados por vírgula", required = true, defaultValue = "VIAT") @QueryParam("siglas") String siglas) throws Exception {
//        Response.ResponseBuilder response = null;
//        CpxINTERFACE ctrl = new CpxCTRL();
//        List ln = ctrl.LRD_DESC(recurso, siglas);
//
//        GenericResponseGetDTO resp = new GenericResponseGetDTO();
//        GenericResponseGetPaginationDTO pag = new GenericResponseGetPaginationDTO();
//
//        pag.setTotalRows(ln.size());
//        resp.setData(ln);
//        resp.setPagination(pag);
//
//        response = Response.ok(resp);
//
//        return response.build();
//    }
}
