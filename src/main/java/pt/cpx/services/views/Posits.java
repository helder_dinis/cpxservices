/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.services.views;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import pt.cpx.ctrl.CpxCTRL;
import pt.cpx.dto.GenericExceptionResponseDTO;
import pt.cpx.dto.GenericResponseGetDTO;
import pt.cpx.dto.GenericResponseGetPaginationDTO;
import pt.cpx.dto.update.PositUpdDTO;
import pt.cpx.interfaces.CpxINTERFACE;

/**
 * REST Web Service
 *
 * @author helder.dinis
 */
@Path("/posits")
@Api(value = "Posits", authorizations = {
    @Authorization(value = "sampleoauth", scopes = {})})
public class Posits {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Posits
     */
    public Posits() {
    }
    
    @GET
    @Path("/{id}")
    @ApiOperation(value = "Retorna um registo", notes = "",
            response = GenericResponseGetDTO.class,
            responseContainer = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok"),
        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
    @Produces({"application/json; charset=UTF-8"})
    public Response getPosit(@PathParam("id") Long id) throws Exception {
        Response.ResponseBuilder response = null;
        CpxINTERFACE ctrl = new CpxCTRL();
        List ln = ctrl.getPosit(id);

        GenericResponseGetDTO resp = new GenericResponseGetDTO();
        GenericResponseGetPaginationDTO pag = new GenericResponseGetPaginationDTO();

        pag.setTotalRows(ln.size());
        resp.setData(ln);
        resp.setPagination(pag);

        response = Response.ok(resp);

        return response.build();
    }

    //http://localhost:8080/cpxServices/webresources/posits?exercicio_id=1&checked_yn=N
    @GET
    @ApiOperation(value = "Retorna uma lista", notes = "", response = GenericResponseGetDTO.class,responseContainer = "")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok"),
        @ApiResponse(code = 400, message = "Bas Request", response = GenericExceptionResponseDTO.class)})
    @Produces({"application/json; charset=UTF-8"})
    public Response getPosits(@ApiParam(value = "", required = true) @QueryParam("exercicio_id") int exercicio_id, @ApiParam(value = "Y/N", required = true) @QueryParam("checked_yn") String checked_yn) throws Exception {
        Response.ResponseBuilder response = null;
        CpxINTERFACE ctrl = new CpxCTRL();
        List ln = ctrl.LPOSIT(exercicio_id, checked_yn);

        GenericResponseGetDTO resp = new GenericResponseGetDTO();
        GenericResponseGetPaginationDTO pag = new GenericResponseGetPaginationDTO();

        pag.setTotalRows(ln.size());
        resp.setData(ln);
        resp.setPagination(pag);

        response = Response.ok(resp);

        return response.build();
    }

    @PUT
    @Path("/{id}")
    @ApiOperation(value = "Altera um registo", notes = "No body, indicar apenas os atributos a alterar<br>"
            + "Exemplo pata o id 6(PathParameter):<br>{\n" +
"      \"participante_id\": 6,\n" +
"      \"checked_yn\": \"Y\",\n" +
"      \"checked_date\": \"21-12-2021 23:23\",\n" +
"      \"updated_by\": \"bruna.fonseca\"\n" +
"} OU <br>{\n" +
"  \"checked_yn\": \"N\",\n" +
"  \"checked_date\":\"\",\n" +
"  \"updated_by\": \"bruno.lage\"\n" +
"}", responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Ok"),
        @ApiResponse(code = 400, message = "[Serão apresentadas várias mensagens de erro consoante o contexto]", response = GenericExceptionResponseDTO.class)})
    public Response alterarPosit(@Context UriInfo uriInfo, @PathParam("id") Long id, @ApiParam(value = "Dados a submeter", required = true) PositUpdDTO row) throws Exception {
        Response.ResponseBuilder response;
        row.setId(id);
        CpxINTERFACE ctrl = new CpxCTRL();
        ctrl.UPOSIT(row);

        response = Response.status(Response.Status.NO_CONTENT); //204

        return response.build();
    }
}
