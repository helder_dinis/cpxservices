package pt.cpx.services.config;

/**
 *
 * @author Ulisses Pinto
 */
import io.swagger.jaxrs.config.BeanConfig;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/webresources")
public class ApplicationConfig extends Application {

    // Configure and Initialize Swagger
    public ApplicationConfig() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setTitle("CPX REST services");
        beanConfig.setDescription("Command Post Exercise Emergency REST services powered by CPX Team Developers<br><br>"
                + "<b>NOTAS:</b><br>"
                + "Dos campos de audit, apenas fazer referência ao <b>created_by</b>(POST) e o <b>updated_by</b>(PUT).<br>"
                + "Ao enviar JSON para o servidor(POST/PUT) apenas indicar os atributos relevantes para a operação em causa.");
        // Scheme and Host are Not required
        // https://www.nvisia.com/insights/swagger-cataloging-with-the-worlds-most-popular-framework-for-apis
        //beanConfig.setSchemes(new String[]{"http", "https"});
        //setHost("localhost:8080");
        beanConfig.setBasePath("/cpxServices/webresources");
        beanConfig.setResourcePackage("pt.cpx");
        beanConfig.setScan(true);
        beanConfig.setPrettyPrint(true);
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(pt.cpx.exceptionHandlingRelated.GenericExceptionMapper.class);
        resources.add(pt.cpx.services.views.Exercicios.class);
        resources.add(pt.cpx.services.views.GeoServices.class);
        resources.add(pt.cpx.services.views.Incidentes.class);
        resources.add(pt.cpx.services.views.Posits.class);
        resources.add(pt.cpx.services.views.Recursos.class);
        resources.add(pt.cpx.services.views.RecursosDisponiveis.class);
        resources.add(pt.cpx.services.views.RecursosPermanentes.class);
        resources.add(pt.cpx.services.views.Security.class);
        resources.add(pt.cpx.services.views.TiposRecursos.class);
    }
}
