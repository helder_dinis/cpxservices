package pt.cpx.ctrl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.UriInfo;
//import org.glassfish.jersey.internal.util.Base64;
import org.apache.commons.codec.binary.Base64;
import pt.cpx.dao.ExercicioDAO;
import pt.cpx.dao.PositDAO;
import pt.cpx.dao.RadioNetworkDAO;
import pt.cpx.dao.RecursoDisponivelDAO;
import pt.cpx.dao.RecursoPermanenteDAO;
import pt.cpx.dao.SecurityDAO;
import pt.cpx.dao.IncidenteDAO;
import pt.cpx.dao.RecursoDAO;
import pt.cpx.dao.TipoRecursoDAO;
import pt.cpx.dto.create.RecursoPermanenteInsDTO;
import pt.cpx.dto.update.PositUpdDTO;
import pt.cpx.dto.update.RecursoPermanenteUpdDTO;
import pt.cpx.exceptionHandlingRelated.GenericException;
import pt.cpx.interfaces.CpxINTERFACE;
import static pt.cpx.utils.Utils.CTRL_EX_MSG;
import static pt.cpx.utils.Utils.*;

public class CpxCTRL implements CpxINTERFACE {

    @Override
    public List LE(int exercicio_id) throws Exception {
        ExercicioDAO dao = new ExercicioDAO();
        List row = new ArrayList<Map<String, Object>>();

        try {
            row = dao.getExercicio(exercicio_id);
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return row;
    }

    @Override
    public List LPOSIT(int exercicio_id, String checked_yn) throws Exception {
        PositDAO dao = new PositDAO();
        List rows = new ArrayList<Map<String, Object>>();
        try {
            if (exercicio_id != 0 && checked_yn != null && (checked_yn.equals("Y") || checked_yn.equals("N"))) {
                rows = dao.getPosit(exercicio_id, checked_yn);
            } else {
                throw new GenericException(QRY_PARAM_EX_HTTP_STATUS_CODE, QRY_PARAM_EX_MSG, null, this.getClass().getSimpleName());
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return rows;
    }

    @Override
    public List getPosit(Long posit_id) throws Exception {
        PositDAO dao = new PositDAO();
        List row = new ArrayList<Map<String, Object>>();

        try {
            row = dao.getPosit(posit_id);
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return row;
    }

    @Override
    public void UPOSIT(PositUpdDTO row) throws Exception {
        PositDAO dao = new PositDAO();
        try {

            if (row.getUpdated_by() == null || row.getUpdated_by().equals("")) {
                throw new GenericException(REQUIRED_FIELD_EX_HTTP_STATUS_CODE, REQUIRED_FIELD_EX_MSG, null, this.getClass().getSimpleName());
            }

            dao.updatePOSIT(row);
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
    }

    @Override
    public List LRP(int recurso_permanente_id) throws Exception { // obeter recurso permanente

        RecursoPermanenteDAO dao = new RecursoPermanenteDAO();
        List row = new ArrayList<Map<String, Object>>();

        try {
            row = dao.getRecurso_Permanente(recurso_permanente_id);
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return row;
    }

    @Override
    public Long CRP(RecursoPermanenteInsDTO row) throws Exception { // criar recurso permanente
        RecursoPermanenteDAO dao = new RecursoPermanenteDAO();
        Long idCreated;
        try {

            if (row.getCreated_by() == null) {
                throw new GenericException(REQUIRED_FIELD_EX_HTTP_STATUS_CODE, REQUIRED_FIELD_EX_MSG, null, this.getClass().getSimpleName());
            }

            idCreated = dao.createRP(row);
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return idCreated;
    }

    @Override
    public void URP(RecursoPermanenteUpdDTO row) throws Exception { // alterar recurso permanente
        RecursoPermanenteDAO dao = new RecursoPermanenteDAO();
        try {

            if (row.getUpdated_by() == null || row.getUpdated_by().equals("")) {
                throw new GenericException(REQUIRED_FIELD_EX_HTTP_STATUS_CODE, REQUIRED_FIELD_EX_MSG, null, this.getClass().getSimpleName());
            }

            dao.updateRP(row);
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
    }

    @Override
    public void ARP(Long recurso_permanente_id) throws Exception {
        RecursoPermanenteDAO dao = new RecursoPermanenteDAO();
        try {
            dao.apagarRP(recurso_permanente_id);
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
    }

    @Override
    public List LRD(UriInfo uriInfo, int exercicio_id, int tipo_recurso_id, String recurso, String siglas) throws Exception {
        RecursoDisponivelDAO dao = new RecursoDisponivelDAO();
        List rows = new ArrayList<Map<String, Object>>();
        List rowsFinal = new ArrayList<Map<String, Object>>();

        try {
            
            if (exercicio_id != 0 || tipo_recurso_id != 0 || !recurso.equals("") || !siglas.equals("")) {
                rows = dao.getRecursoDisponivel(exercicio_id, tipo_recurso_id, recurso, siglas);

                for (int i = 0; i < rows.size(); i++) {// incluir fotos
                    
                    Map<String, Object> row = new HashMap<>();
                    row.putAll((Map<? extends String, ? extends Object>) rows.get(i));
                    //String foto = uriInfo.getBaseUri().toString() + "recursos/"+row.get("ID")+"/foto";
                    String foto = "https://cpxzone.xyz/cpxServices/webresources/recursos/"+row.get("ID")+"/foto";
                    row.put("FOTO_URL", foto);
                    rowsFinal.add(row);
                }

            } else {
                throw new GenericException(QRY_PARAM_EX_HTTP_STATUS_CODE, QRY_PARAM_EX_MSG, null, this.getClass().getSimpleName());
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return rowsFinal;
    }

    @Override
    public void CI(int exercicio_id
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void UI(int insidente_id
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List LI(int exercicio_id) throws Exception {
        IncidenteDAO dao = new IncidenteDAO();
        List rows = new ArrayList<Map<String, Object>>();
        try {
            if (exercicio_id != 0) {
                rows = dao.getIncidentes(exercicio_id);
            } else {
                throw new GenericException(QRY_PARAM_EX_HTTP_STATUS_CODE, QRY_PARAM_EX_MSG, null, this.getClass().getSimpleName());
            }
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return rows;
    }

    @Override
    public List LR(String network) throws Exception {
        RadioNetworkDAO rn = new RadioNetworkDAO();

        List rows = new ArrayList<Map<String, Object>>();
        try {

            if (network != null) { // só vai à bd se há critério pesquisa
                rows = rn.getNetwork(network);
            } else {
                throw new GenericException(QRY_PARAM_EX_HTTP_STATUS_CODE, QRY_PARAM_EX_MSG, null, this.getClass().getSimpleName());
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return rows;
    }

    @Override
    public List LE(String typeStructures
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List LTR() throws Exception {
        TipoRecursoDAO tr = new TipoRecursoDAO();

        List rows = new ArrayList<Map<String, Object>>();
        try {
            rows = tr.getTiposRecursos();
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return rows;
    }

    @Override
    public Map<String, Object> LRF(int recurso_id) throws Exception {
        RecursoDAO r = new RecursoDAO();
        Blob blob = null;
        byte[] bytes = null;
        Map<String, Object> img = new HashMap<>();
        List<Map<String, Object>> row = null;
        try {
            row = r.getFile(recurso_id);

            if (row != null && row.size() > 0) {
                blob = (Blob) row.get(0).get("CONTENT");

                int blobLength = (int) blob.length();
                int pos = 1; // position is 1-based
                bytes = blob.getBytes(pos, blobLength);
            }

            img.put("bin", bytes);
            img.put("mimeType", row.get(0).get("MIME_TYPE"));

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return img;
    }

    @Override
    public List canAccessMap(long session) throws Exception {
        SecurityDAO dao = new SecurityDAO();
        List row = new ArrayList<Map<String, String>>();
        try {
//            if (session == 123456789) { // bruna
//                row.put("username", "BRUNA.FONSECA");
//            } else {
            row = dao.canAccessMap(session);
            //}
            if (row.isEmpty()) {
                throw new GenericException(UNAUTHORIZED_HTTP_STATUS_CODE, UNAUTHORIZED_MSG, null, this.getClass().getSimpleName());
            }
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(CTRL_EX_HTTP_STATUS_CODE, CTRL_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return row;
    }
}
