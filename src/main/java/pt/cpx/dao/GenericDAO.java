/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import pt.cpx.exceptionHandlingRelated.GenericException;
import pt.cpx.utils.Utils;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.DB_CONN_EX_MSG;
import static pt.cpx.utils.Utils.GET_DATA_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.GET_DATA_EX_MSG;

/**
 *
 * @author helder.dinis
 */
public class GenericDAO {

    public java.sql.Connection getConnectionCPX() throws Exception {
        Context c = new InitialContext();
        try {
            DataSource d = (DataSource) c.lookup("jdbc/app_cpx");
            //d.getConnection().getMetaData(); // checar qq coisa p saber se bd está acessivel
            return d.getConnection();

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DB_CONN_EX_MSG, ex, Utils.class.getSimpleName());
            }
        }

    }

    public boolean idExists(Long id, String tn, java.sql.Connection c) throws Exception {
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        boolean exists = false;
        try {

            String q = "select id from app_cpx." + tn + " where id = ?";

            ps = c.prepareStatement(q);
            ps.setLong(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                exists = true;
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(500, "Erro ao verificar se o ID existe", ex, "Utils");
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return exists;
    }

    public void setParams(PreparedStatement pstmt, Object[] params) throws SQLException {
        if (params != null) {
            final int tam = params.length;
            for (int i = 1; i < tam; i++) {
                Object param = params[i];
                //if (param != null) {
                pstmt.setObject(i, param);
                //}
            }
        }
    }

    public ResultSet query(PreparedStatement ps, Object[] params) throws SQLException, Exception {
        if (params != null) {
            setParams(ps, params);
        }
        return ps.executeQuery();
    }

    public List getData(String q, Object[] params) throws SQLException, Exception {
        java.sql.Connection c = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List rows = new ArrayList<Map<String, Object>>();

        try {
            c = this.getConnectionCPX();
            ps = c.prepareStatement(q);
            rs = this.query(ps, params);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            while (rs.next()) {
                Map<String, Object> row = new LinkedHashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String name = rsmd.getColumnName(i);
                    row.put(name, rs.getObject(name));
                }
                rows.add(row);
            }
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(GET_DATA_EX_HTTP_STATUS_CODE, GET_DATA_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (c != null) {
                c.close();
            }

        }
        return rows;
    }
}
