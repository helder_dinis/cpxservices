/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pt.cpx.exceptionHandlingRelated.GenericException;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.DAO_EX_MSG;

/**
 *
 * @author helder.dinis
 */
public class TipoRecursoDAO extends GenericDAO {

    public TipoRecursoDAO() {
    }

    public List getTiposRecursos() throws Exception {
        List rows = new ArrayList<Map<String, Object>>();

        try {
            String q = "select * from app_cpx.tipo_recurso ";
            rows.addAll(this.getData(q, null));
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return rows;
    }
}
