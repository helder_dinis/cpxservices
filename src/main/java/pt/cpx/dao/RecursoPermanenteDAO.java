/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dao;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import oracle.jdbc.OracleTypes;
import pt.cpx.dto.create.RecursoPermanenteInsDTO;
import pt.cpx.exceptionHandlingRelated.GenericException;
import pt.cpx.utils.Utils;
import static pt.cpx.utils.Utils.DAO_EX_MSG;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.ID_NOT_FOUND_EX_MSG;
import static pt.cpx.utils.Utils.ID_NOT_FOUND_EX_STATUS_CODE;
import static pt.cpx.utils.Utils.NUMERIC_EXPECTED_EX_MSG;
import static pt.cpx.utils.Utils.NUMERIC_EXPECTED_EX_STATUS_CODE;

/**
 *
 * @author helder.dinis
 */
public class RecursoPermanenteDAO {

    public RecursoPermanenteDAO() {
    }

    public List getRecurso_Permanente(long id) throws Exception {
        java.sql.Connection c = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List row = new ArrayList<Map<String, Object>>();

        try {

            c = Utils.getConnectionCPX();
            String q = "select * from app_cpx.Recurso_Permanente where id = ?";

            ps = c.prepareStatement(q);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            while (rs.next()) {
                Map<String, Object> col = new LinkedHashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String name = rsmd.getColumnName(i);
                    col.put(name, rs.getString(name));
                }
                row.add(col);
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (c != null) {
                c.close();
            }
        }
        return row;
    }

    public Long createRP(pt.cpx.dto.create.RecursoPermanenteInsDTO row) throws Exception {
        java.sql.Connection c = null;
        java.sql.CallableStatement cs = null;
        java.sql.ResultSet rs = null;
        Long idCreated = null;
        try {
            c = Utils.getConnectionCPX();
            String q = "DECLARE "
                    + "   row          RECURSO_PERMANENTE%ROWTYPE; "
                    + "BEGIN "
                    + "   row.exercicio_id := ?; "
                    + "   row.recurso_id := ?; "
                    + "   row.descricao := ?; "
                    + "   row.lat := ?; "
                    + "   row.lng := ?; "
                    + "   row.created_by := upper(?); "
                    + "   pkg_dml_RECURSO_PERMANENTE.ins (row, ?,?,?,?); "
                    + "END;";

            cs = c.prepareCall(q);

            if (row.getExercicio_id() == null) {
                cs.setNull(1, Types.DOUBLE);
            } else {
                cs.setLong(1, row.getExercicio_id());
            }
            if (row.getRecurso_id() == null) {
                cs.setNull(2, Types.DOUBLE);
            } else {
                cs.setLong(2, row.getRecurso_id());
            }
            if (row.getDescricao() == null) {
                cs.setNull(3, Types.VARCHAR);
            } else {
                cs.setString(3, row.getDescricao());
            }
            if (row.getLat() == null) {
                cs.setNull(4, Types.VARCHAR);
            } else {
                cs.setString(4, row.getLat());
            }
            if (row.getLng() == null) {
                cs.setNull(5, Types.VARCHAR);
            } else {
                cs.setString(5, row.getLng());
            }
            cs.setString(6, row.getCreated_by());
            cs.registerOutParameter(7, OracleTypes.NUMBER);  // id created
            cs.registerOutParameter(8, OracleTypes.VARCHAR); // err_code
            cs.registerOutParameter(9, OracleTypes.VARCHAR); // err_to_log
            cs.registerOutParameter(10, OracleTypes.VARCHAR); // err_msg_frendly 

            // run SP
            cs.execute();
            String errCode = cs.getString(8);
            if (errCode != null) { // erro na bd
                String logMsg = cs.getString(9);
                String errMsgFrendly = cs.getString(10);
                Throwable dbEx = null;
                if (logMsg != null) {// há mensagem p log
                    dbEx = new Throwable(logMsg);
                }
                throw new GenericException(400, errMsgFrendly, dbEx, this.getClass().getSimpleName());
            }
            idCreated = cs.getLong(7);
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (c != null) {
                c.close();
            }
        }
        return idCreated;

    }

    public void updateRP(pt.cpx.dto.update.RecursoPermanenteUpdDTO row) throws Exception {
        java.sql.Connection c = null;
        java.sql.CallableStatement cs = null;
        java.sql.ResultSet rs = null;
        try {
            c = Utils.getConnectionCPX();

            String q = "DECLARE "
                    + "   row           RECURSO_PERMANENTE%ROWTYPE; "
                    + "   row2update   RECURSO_PERMANENTE%ROWTYPE;"
                    + "BEGIN "
                    + "   row.id := ?; "
                    + ""
                    + "   SELECT * "
                    + "     INTO row2update "
                    + "     FROM RECURSO_PERMANENTE "
                    + "    WHERE id = row.id; "
                    + "";

            if (row.getExercicio_id() != null) {
                q = q + "   row2update.exercicio_id := ?; ";
            }

            if (row.getRecurso_id() != null) {
                q = q + "   row2update.recurso_id := ?; ";
            }

            if (row.getDescricao() != null) {
                q = q + "   row2update.descricao := ?; ";
            }
            if (row.getLat() != null) {
                q = q + "   row2update.lat := ?; ";
            }
            if (row.getLng() != null) {
                q = q + "   row2update.lng := ?; ";
            }
            if (row.getUpdated_by() != null) {
                q = q + "   row2update.updated_by := ?; ";
            }
            q = q + "   pkg_dml_RECURSO_PERMANENTE.upd (row2update,?,?,?); "
                    + "END; ";

            cs = c.prepareCall(q);

            int i = 1;

            cs.setLong(i, row.getId());
            if (row.getExercicio_id() != null) {
                i++;
                cs.setLong(i, row.getExercicio_id());
            }

            if (row.getRecurso_id() != null) {
                i++;
                cs.setLong(i, row.getRecurso_id());
            }

            if (row.getDescricao() != null) {
                i++;
                cs.setString(i, row.getDescricao());
            }
            if (row.getLat() != null) {
                i++;
                cs.setString(i, row.getLat());
            }
            if (row.getLng() != null) {
                i++;
                cs.setString(i, row.getLng());
            }
            if (row.getUpdated_by() != null) {
                i++;
                cs.setString(i, row.getUpdated_by());
            }
            i++;
            cs.registerOutParameter(i, OracleTypes.VARCHAR); // err_code 
            i++;
            cs.registerOutParameter(i, OracleTypes.VARCHAR); // err_to_log
            i++;
            cs.registerOutParameter(i, OracleTypes.VARCHAR); // err_msg_frendly

            // run SP
            cs.execute();
            String errCode = cs.getString(i - 2);
            if (errCode != null) { // erro na bd
                String logMsg = cs.getString(i - 1);
                String errMsgFrendly = cs.getString(i);
                Throwable dbEx = null;
                if (logMsg != null) {// há mensagem p log
                    dbEx = new Throwable(logMsg);
                }
                throw new GenericException(400, errMsgFrendly, dbEx, this.getClass().getSimpleName());
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else if (ex instanceof SQLException) {
                if (((SQLException) ex).getErrorCode() == 1403) { // id n encontrado
                    throw new GenericException(ID_NOT_FOUND_EX_STATUS_CODE, ID_NOT_FOUND_EX_MSG, null, this.getClass().getSimpleName());
                } else if (((SQLException) ex).getErrorCode() == 1858) { // esperado numerico...
                    throw new GenericException(NUMERIC_EXPECTED_EX_STATUS_CODE, NUMERIC_EXPECTED_EX_MSG, null, this.getClass().getSimpleName());
                } else {
                    throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
                }
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (c != null) {
                c.close();
            }
        }
    }

    public void apagarRP(Long recurso_permanente_id) throws Exception {
        java.sql.Connection c = null;
        java.sql.CallableStatement cs = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List row = new ArrayList<Map<String, Object>>();

        try {

            c = Utils.getConnectionCPX();

            String q = "BEGIN "
                    + "   pkg_dml_RECURSO_PERMANENTE.del (?,?,?,?); "
                    + "END;";
            cs = c.prepareCall(q);
            cs.setLong(1, recurso_permanente_id);
            cs.registerOutParameter(2, OracleTypes.VARCHAR); // err_code
            cs.registerOutParameter(3, OracleTypes.VARCHAR); // err_to_log
            cs.registerOutParameter(4, OracleTypes.VARCHAR); // err_msg_frendly

            // run SP
            cs.execute();

            String errCode = cs.getString(2); // ver se houve erro
            if (errCode != null) { // erro na bd
                String errToLog = cs.getString(3);
                String errMsgFrendly = cs.getString(4);
                Throwable dbEx = null;
                if (errToLog != null) { // há mensagem p log
                    dbEx = new Throwable(errToLog);
                }
                throw new GenericException(400, errMsgFrendly, dbEx, this.getClass().getSimpleName());
            }
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (c != null) {
                c.close();
            }
        }
    }
}
