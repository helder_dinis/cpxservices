/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dao;

import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pt.cpx.exceptionHandlingRelated.GenericException;
import pt.cpx.utils.Utils;
import static pt.cpx.utils.Utils.DAO_EX_MSG;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;


/**
 *
 * @author helder.dinis
 */
public class ExercicioDAO {

    public ExercicioDAO() {
    }

    public List getExercicio(long id) throws Exception {
        java.sql.Connection c = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List row = new ArrayList<Map<String, Object>>();

        try {

            c = Utils.getConnectionCPX();
            String q = "select * from app_cpx.exercicio where id = ?";

            ps = c.prepareStatement(q);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            while (rs.next()) {
                Map<String, Object> col = new LinkedHashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String name = rsmd.getColumnName(i);
                    col.put(name, rs.getString(name));
                }
                row.add(col);
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG,ex,this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (c != null) {
                c.close();
            }
        }
        return row;
    }
}
