/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dao;

import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pt.cpx.exceptionHandlingRelated.GenericException;
import pt.cpx.utils.Utils;
import static pt.cpx.utils.Utils.DAO_EX_MSG;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;

/**
 *
 * @author helder.dinis
 */
public class SecurityDAO {

    public SecurityDAO() {
    }

    public List canAccessMap(long session) throws Exception {
        java.sql.Connection c = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List row = new ArrayList<Map<String, String>>();
        try {

            c = Utils.getConnectionCPX();
            String q = ""
                    + "SELECT t.user_name,p.* "
                    + "  FROM apex_workspace_sessions t, utilizador u, participante p "
                    + " WHERE     t.apex_session_id = ? "
                    + "       AND UPPER (t.user_name) = UPPER (u.login) "
                    + "       AND u.id = p.utilizador_id "
                    + "       and activo_yn = 'Y'";
            ps = c.prepareStatement(q);
            ps.setLong(1, session);
            rs = ps.executeQuery();

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            Map<String, String> user = new HashMap<String, String>();
            Map<String, String> participante = new HashMap<String, String>();
            while (rs.next()) {
                Map<String, Object> col = new LinkedHashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String name = rsmd.getColumnName(i);
                    String val = rs.getString(name);
                    if (val == null) {
                        val = "";
                    }
                    if (name.equals("user_name")) {
                        user.put("username", val);
                    } else {
                        participante.put(name, val);
                    }
                }
                row.add(user);
                row.add(participante);
            }
            if (row.size() == 0 && session == 123456789) {
                
                Map<String, Object> col = new HashMap<String, Object>();
                col.put("USERNAME", "BRUNA.FONSECA");
                col.put("ID", "6" );
                col.put("EXERCICIO_ID", "1" );
                col.put("UTILIZADOR_ID", "8" );
                col.put("PERFIL_ID", "2" );
                col.put("FUNCAO_ID", "" );
                col.put("PRESENTE", "Y" );
                

                row.add(col);
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (c != null) {
                c.close();
            }
        }
        return row;

    }
}
