/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pt.cpx.exceptionHandlingRelated.GenericException;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.DAO_EX_MSG;

/**
 *
 * @author helder.dinis
 */
public class RecursoDAO extends GenericDAO {

    public List getFile(int recurso_id) throws Exception {
        List row = new ArrayList<Map<String, Object>>();
        try {
            String q = "select * from app_cpx.RECURSO where ";
            Object[] params = null;
            int maxParams = 1;
            maxParams = recurso_id != 0 ? maxParams + 1 : maxParams;
            params = new Object[maxParams];
            int idxParam = 1;
            String oper = "";
            if (recurso_id != 0) {
                params[idxParam] = recurso_id;
                idxParam++;
                q = q + oper + " id =? ";
                oper = " and ";
            }
            row.addAll(this.getData(q, params));
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return row;
    }

}
