/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dao;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import pt.cpx.exceptionHandlingRelated.GenericException;
import pt.cpx.utils.Utils;
import java.util.LinkedHashMap;
import java.util.List;
import oracle.jdbc.OracleTypes;
import pt.cpx.dto.update.PositUpdDTO;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.DAO_EX_MSG;
import static pt.cpx.utils.Utils.ID_NOT_FOUND_EX_MSG;
import static pt.cpx.utils.Utils.ID_NOT_FOUND_EX_STATUS_CODE;
import static pt.cpx.utils.Utils.INVALID_DATE_EX_MSG;
import static pt.cpx.utils.Utils.INVALID_DATE_EX_STATUS_CODE;
import static pt.cpx.utils.Utils.NUMERIC_EXPECTED_EX_MSG;
import static pt.cpx.utils.Utils.NUMERIC_EXPECTED_EX_STATUS_CODE;

/**
 *
 * @author helder.dinis
 */
public class PositDAO {

    public PositDAO() {
    }

    public List<Map<String, Object>> getPosit(int id, String checked_yd) throws Exception {
        java.sql.Connection c = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List rows = new ArrayList<Map<String, Object>>();

        try {
            c = Utils.getConnectionCPX();
            String q = "select * from app_cpx.posit where exercicio_id = ? and checked_yn = ?";
            ps = c.prepareStatement(q);
            ps.setLong(1, id);
            ps.setString(2, String.valueOf(checked_yd));
            rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            while (rs.next()) {
                Map<String, Object> col = new LinkedHashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String name = rsmd.getColumnName(i);
                    col.put(name, rs.getString(name));
                }
                rows.add(col);
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (c != null) {
                c.close();
            }
        }
        return rows;
    }

    public List getPosit(long id) throws Exception {
        java.sql.Connection c = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List row = new ArrayList<Map<String, Object>>();

        try {

            c = Utils.getConnectionCPX();
            String q = "select * from app_cpx.posit where id = ?";

            ps = c.prepareStatement(q);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            while (rs.next()) {
                Map<String, Object> col = new LinkedHashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String name = rsmd.getColumnName(i);
                    String val = rs.getString(name);
                    if (val == null) {
                        val = "";
                    }
                    col.put(name, val);
                }
                row.add(col);
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (c != null) {
                c.close();
            }
        }
        return row;
    }

    public void updatePOSIT(PositUpdDTO row) throws Exception {
        java.sql.Connection c = null;
        java.sql.CallableStatement cs = null;
        java.sql.ResultSet rs = null;
        try {
            c = Utils.getConnectionCPX();

            String q = "DECLARE "
                    + "   row           POSIT%ROWTYPE; "
                    + "   row2update   POSIT%ROWTYPE;"
                    + "BEGIN "
                    + "   row.id := ?; "
                    + ""
                    + "   SELECT * "
                    + "     INTO row2update "
                    + "     FROM POSIT "
                    + "    WHERE id = row.id; "
                    + "";

            if (row.getIncidente_id() != null) {
                q = q + "row2update.incidente_id := ?; ";

            }

            if (row.getParticipante_id() != null) {
                q = q + "row2update.participante_id := ?; ";
            }

            if (row.getDescricao() != null) {
                q = q + "row2update.descricao := ?; ";
            }

            if (row.getData() != null) {
                q = q + "row2update.data := to_date(?,'dd-mm-yyyy'); ";
            }

            if (row.getFuncao_id() != null) {
                q = q + "row2update.funcao_id := ?; ";
            }

            if (row.getExercicio_id() != null) {
                q = q + "   row2update.exercicio_id := ?; ";
            }

            if (row.getChecked_yn() != null) {
                q = q + "   row2update.checked_yn := ?; ";
            }
            if (row.getChecked_date() != null) {
                q = q + "   row2update.checked_date := to_date(?,'dd-mm-yyyy HH24:MI'); ";
            }
            if (row.getUpdated_by() != null) {
                q = q + "   row2update.updated_by := ?; ";
            }
            q = q + "   pkg_dml_posit.upd (row2update,?,?,?); "
                    + "END; ";

            cs = c.prepareCall(q);

            int i = 1;

            cs.setLong(i, row.getId());
            if (row.getIncidente_id() != null) {
                i++;
                cs.setLong(i, row.getIncidente_id());
            }
            if (row.getParticipante_id() != null) {
                i++;
                cs.setLong(i, row.getParticipante_id());
            }
            if (row.getDescricao() != null) {
                i++;
                cs.setString(i, row.getDescricao());
            }
            if (row.getData() != null) {
                i++;
                cs.setString(i, row.getData());
            }
            if (row.getFuncao_id() != null) {
                i++;
                cs.setLong(i, row.getFuncao_id());
            }
            if (row.getExercicio_id() != null) {
                i++;
                cs.setLong(i, row.getExercicio_id());
            }
            if (row.getChecked_yn() != null) {
                i++;
                cs.setString(i, row.getChecked_yn());
            }
            if (row.getChecked_date() != null) {
                i++;
                cs.setString(i, row.getChecked_date());
            }
            if (row.getUpdated_by() != null) {
                i++;
                cs.setString(i, row.getUpdated_by());
            }

            i++;
            cs.registerOutParameter(i, OracleTypes.VARCHAR); // err_code 
            i++;
            cs.registerOutParameter(i, OracleTypes.VARCHAR); // err_to_log
            i++;
            cs.registerOutParameter(i, OracleTypes.VARCHAR); // err_msg_frendly

            // run SP
            cs.execute();
            String errCode = cs.getString(i - 2);
            if (errCode != null) { // erro na bd
                String logMsg = cs.getString(i - 1);
                String errMsgFrendly = cs.getString(i);
                Throwable dbEx = null;
                if (logMsg != null) {// há mensagem p log
                    dbEx = new Throwable(logMsg);
                }
                throw new GenericException(400, errMsgFrendly, dbEx, this.getClass().getSimpleName());
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else if (ex instanceof SQLException) {
                if (((SQLException) ex).getErrorCode() == 1403) { // id n encontrado
                    throw new GenericException(ID_NOT_FOUND_EX_STATUS_CODE, ID_NOT_FOUND_EX_MSG, null, this.getClass().getSimpleName());
                } else if (((SQLException) ex).getErrorCode() == 1858) { // esperado numerico
                    throw new GenericException(NUMERIC_EXPECTED_EX_STATUS_CODE, NUMERIC_EXPECTED_EX_MSG, null, this.getClass().getSimpleName());
                } else if (((SQLException) ex).getErrorCode() == 1830) { // formato data inválido
                    throw new GenericException(INVALID_DATE_EX_STATUS_CODE, INVALID_DATE_EX_MSG, null, this.getClass().getSimpleName());

                } else {
                    ex.getCause();
                    throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
                }
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (c != null) {
                c.close();
            }
        }
    }
}
