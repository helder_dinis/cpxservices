package pt.cpx.dao;

import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pt.cpx.exceptionHandlingRelated.GenericException;
import pt.cpx.utils.Utils;
import static pt.cpx.utils.Utils.DAO_EX_MSG;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;

public class RadioNetworkDAO {

    public List<Map<String, Object>> getNetwork(String network) throws Exception {
        java.sql.Connection c = null;
        java.sql.PreparedStatement ps = null;
        java.sql.ResultSet rs = null;
        List<Map<String, Object>> listSites = new ArrayList<Map<String, Object>>();
        try {

            c = Utils.getConnectionCPX();
            String q = "select id\n"
                    + "     , network\n"
                    + "     , site_name\n"
                    + "     , site_code\n"
                    + "     , description\n"
                    + "     , localization\n"
                    + "     , district\n"
                    + "     , lat\n"
                    + "     , lng\n"
                    + "     , tipo_status_id\n"
                    + "  from app_cpx.radio_network\n"
                    + " where network = ?";

            ps = c.prepareStatement(q);
            ps.setString(1, network);
            rs = ps.executeQuery();

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();

            while (rs.next()) {
                Map<String, Object> maps = new LinkedHashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String name = rsmd.getColumnName(i);
                    maps.put(name, rs.getString(name));
                }
                listSites.add(maps);
            }

        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG,ex,this.getClass().getSimpleName());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (c != null) {
                c.close();
            }
        }
        return listSites;
    }

}
