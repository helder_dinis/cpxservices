/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dao;

import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pt.cpx.exceptionHandlingRelated.GenericException;
import static pt.cpx.utils.Utils.DAO_EX_HTTP_STATUS_CODE;
import static pt.cpx.utils.Utils.DAO_EX_MSG;


/**
 *
 * @author helder.dinis
 */
public class RecursoDisponivelDAO extends GenericDAO{

    public RecursoDisponivelDAO() {
    }

    public List getRecursoDisponivel(int exercicio_id, int tipo_recurso_id, String recurso, String siglas) throws Exception {
        List rows = new ArrayList<Map<String, Object>>();
        try {
            String q = "select * from app_cpx.RECURSO_DISPONIVEL_EXERCICIO where ";
            Object[] params = null;
            int maxParams = 1;
            maxParams = exercicio_id != 0 ? maxParams + 1 : maxParams;
            maxParams = tipo_recurso_id != 0 ? maxParams + 1 : maxParams;
            maxParams = recurso != null && !recurso.equals("") ? maxParams + 1 : maxParams;
            maxParams = siglas != null && !siglas.equals("") ? maxParams + siglas.split(",").length : maxParams;
            params = new Object[maxParams];
            int idxParam = 1;
            String oper = "";
            if (exercicio_id != 0) {
                params[idxParam] = exercicio_id;
                idxParam++;
                q = q + oper + " exercicio_id =? ";
                oper = " and ";
            }
            if (tipo_recurso_id != 0) {
                params[idxParam] = tipo_recurso_id;
                idxParam++;
                q = q + oper + " tipo_recurso_id =? ";
                oper = " and ";
            }
            if (recurso != null && !recurso.equals("")) {
                params[idxParam] = "%" + recurso.replace(" ", "%") + "%";
                idxParam++;
                q = q + oper + " UPPER(recurso) like UPPER(?) ";
                oper = " and ";
            }
            if (siglas != null && !siglas.equals("")) {
                String[] sgs = siglas.split(",");
                if (sgs.length == 1) { // uma sigla
                    params[idxParam] = siglas;
                    idxParam++;
                    q = q + oper + " sigla in (upper(?)) ";
                    oper = " and ";
                } else {
                    q = q + oper + " sigla in (";
                    String v = "";
                    for (int i = 0; i < sgs.length; i++) {
                        params[idxParam] = sgs[i];
                        idxParam++;
                        q = q + v + "upper(?)";
                        v = ",";
                    }
                    q = q + ")";
                }
            }
            rows.addAll(this.getData(q, params));
        } catch (Exception ex) {
            if (ex instanceof GenericException) {
                throw ex;
            } else {
                throw new GenericException(DAO_EX_HTTP_STATUS_CODE, DAO_EX_MSG, ex, this.getClass().getSimpleName());
            }
        }
        return rows;
    }
}
