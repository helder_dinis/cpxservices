package pt.cpx.interfaces;

import java.util.List;
import java.util.Map;
import javax.ws.rs.core.UriInfo;
import pt.cpx.dto.create.RecursoPermanenteInsDTO;
import pt.cpx.dto.update.PositUpdDTO;
import pt.cpx.dto.update.RecursoPermanenteUpdDTO;

public interface CpxINTERFACE {

    //
    // Exercicios
    //
    List LE(int exercicio_id) throws Exception; // listar exercício

    //
    // Posits
    //
    List LPOSIT(int exercicio_id, String checked_yn) throws Exception; // listar posits

    List getPosit(Long id) throws Exception; // listar posits

    void UPOSIT(PositUpdDTO row) throws Exception; // alterar posits

    //
    // Recursos Permanentes
    //
    List LRP(int recurso_permanente_id) throws Exception; //obter

    Long CRP(RecursoPermanenteInsDTO row) throws Exception; // inserir

    void URP(RecursoPermanenteUpdDTO row) throws Exception; // alterar

    void ARP(Long recurso_permanente_id) throws Exception; //apagar

    //
    // Recursos Disponiveis
    //
    List LRD(UriInfo uriInfo, int exercicio_id, int tipo_recurso_id, String recurso, String siglas) throws Exception; // listar

    //
    // Incidentes
    //
    void CI(int exercicio_id); //criar incidente

    void UI(int insidente_id); //actualizar insidente

    List LI(int exercicio_id) throws Exception; //listar insidentes

    //
    // Radios Network
    //
    List LR(String network) throws Exception; //listar radios

    //
    // Estruturas Apoio
    //
    List LE(String typeStructures); // listar estruturas

    //
    // Tipos de Recurso
    //
    List LTR()  throws Exception; // listar tipos recursos

    //
    // Recurso
    //
    Map<String,Object> LRF(int recurso_id)  throws Exception; // listar tipos recursos
    
    //
    // Segurança
    //
    List canAccessMap(long session) throws Exception; // acesso ao mapa

}
