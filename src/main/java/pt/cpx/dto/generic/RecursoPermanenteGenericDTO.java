/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dto.generic;

/**
 *
 * @author helder.dinis
 */
public class RecursoPermanenteGenericDTO {

    private Long id;
    private Long exercicio_id;
    private Long recurso_id;
    private String descricao;
    private String lat;
    private String lng;

    public RecursoPermanenteGenericDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExercicio_id() {
        return exercicio_id;
    }

    public void setExercicio_id(Long exercicio_id) {
        this.exercicio_id = exercicio_id;
    }

    public Long getRecurso_id() {
        return recurso_id;
    }

    public void setRecurso_id(Long recurso_id) {
        this.recurso_id = recurso_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
