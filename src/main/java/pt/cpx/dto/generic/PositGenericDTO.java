/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dto.generic;

import pt.cpx.dto.update.*;
import pt.cpx.dto.*;

/**
 *
 * @author helder.dinis
 */
public class PositGenericDTO {

    private Long id;
    private Long incidente_id;
    private Long participante_id;
    private String descricao;
    private String data;
    private Long funcao_id;
    private Long exercicio_id;
    private String checked_yn;
    private String checked_date;

    public PositGenericDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIncidente_id() {
        return incidente_id;
    }

    public void setIncidente_id(Long incidente_id) {
        this.incidente_id = incidente_id;
    }

    public Long getParticipante_id() {
        return participante_id;
    }

    public void setParticipante_id(Long participante_id) {
        this.participante_id = participante_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getFuncao_id() {
        return funcao_id;
    }

    public void setFuncao_id(Long funcao_id) {
        this.funcao_id = funcao_id;
    }

    public Long getExercicio_id() {
        return exercicio_id;
    }

    public void setExercicio_id(Long exercicio_id) {
        this.exercicio_id = exercicio_id;
    }

    public String getChecked_yn() {
        return checked_yn;
    }

    public void setChecked_yn(String checked_yn) {
        this.checked_yn = checked_yn;
    }

    public String getChecked_date() {
        return checked_date;
    }

    public void setChecked_date(String checked_date) {
        this.checked_date = checked_date;
    }
}
