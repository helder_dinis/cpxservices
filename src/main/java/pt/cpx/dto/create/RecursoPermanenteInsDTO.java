/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dto.create;

import pt.cpx.dto.generic.RecursoPermanenteGenericDTO;

/**
 *
 * @author helder.dinis
 */
public class RecursoPermanenteInsDTO extends RecursoPermanenteGenericDTO {

    private String created_by;

    public RecursoPermanenteInsDTO() {
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }
}
