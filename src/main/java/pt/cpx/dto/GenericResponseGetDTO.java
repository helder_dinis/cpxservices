/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dto;

import java.util.ArrayList;

/**
 *
 * @author helder.dinis
 */
public class GenericResponseGetDTO {

    private GenericResponseGetPaginationDTO pagination;
    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public GenericResponseGetDTO() {
    }

    public GenericResponseGetPaginationDTO getPagination() {
        return pagination;
    }

    public void setPagination(GenericResponseGetPaginationDTO pagination) {
        this.pagination = pagination;
    }

}
