/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dto;

/**
 *
 * @author helder.dinis
 */
public class GenericResponseGetPaginationDTO {

    private long totalRows;

    public GenericResponseGetPaginationDTO() {
    }

    public long getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(long totalRows) {
        this.totalRows = totalRows;
    }

}
