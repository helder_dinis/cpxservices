/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cpx.dto.update;

import pt.cpx.dto.generic.PositGenericDTO;

/**
 *
 * @author helder.dinis
 */
public class PositUpdDTO extends PositGenericDTO {

    private String updated_by;


    public PositUpdDTO() {
    }
    
    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }
}
